import os
import sqlite3


class PetStoreDB:

    @classmethod
    def fetch_all(cls, query):
        pass

    @classmethod
    def insert(cls, query):
        pass

    @classmethod
    def init_db(cls):
        with sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + '/pet_store.sqlite') as conn:
            cursor = conn.cursor()
            cursor.execute(
                '''CREATE TABLE IF NOT EXISTS PETS (
                ID INT PRIMARY KEY NOT NULL,
                NAME TEXT NOT NULL,
                ANIMAL_TYPE TEXT NOT NULL,
                CREATED TEXT NOT NULL);
                '''
            )
            cursor.execute(
                '''INSERT OR IGNORE INTO PETS (
                ID,NAME,ANIMAL_TYPE,CREATED) 
                VALUES (1, 'Humi', 'dog', '01-01-2019');
                '''
            )
            conn.commit()

    @classmethod
    def get_pets(cls, limit, animal_type=None):
        pass
